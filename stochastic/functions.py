from numpy import pi
from numpy import cos
from numpy import linspace
from numpy import meshgrid


class BaseFunction(object):
    def __init__(self, a=1, b=2, c=0.3, d=0.4, alpha=3*pi, gamma=4*pi):
        self.a = a
        self.b = b
        self.c = c
        self.d = d
        self.alpha = alpha
        self.gamma = gamma

    def __call__(self, *args):
        return 0.0


class TestFunctionOne(BaseFunction):
    def __call__(self, *args):
        x, y = args
        linear = self.a * x ** 2 + self.b * y ** 2
        trigon = self.c * cos(self.alpha * x) + self.d * cos(self.gamma * y)
        consta = self.c + self.d
        return linear - trigon + consta


if __name__ == "__main__":
    phi = TestFunctionOne(a=1)
    x = linspace(-1, 1, 500)
    y = linspace(-1, 1, 500)
    X, Y = meshgrid(x, y)
    print(phi(X, Y))
