from numpy import array
from numpy import delete
from numpy import diag
from numpy import mean
from numpy import zeros
from numpy import random

import logging


def move_towards(source, target, amount):
    direction = target - source
    return source + amount * direction


def interest_points(values):
    svalues = sorted(enumerate(values), key=lambda t: t[1])
    return svalues[0][0], svalues[-1][0], svalues[-2][0]


def nelder_and_mead(function, domain, maxiter=200):
    '''
    Domain should be a hipercube defined as (lower_corner, higher_corner)
    lower_corner = [alpha_1, alpha_2, ...]
    higher_corner = [beta_1, beta_2, ...]
    '''
    lower_corner, higher_corner = domain
    lower_corner = array(lower_corner)
    higher_corner = array(higher_corner)
    if len(lower_corner) != len(higher_corner):
        raise ValueError('corner dimensions must be the same')
    if any(higher_corner - lower_corner < 0):
        raise ValueError('Are you forreal?')
    dimensions = len(lower_corner)
    span = higher_corner - lower_corner
    simplex = zeros((dimensions + 1, dimensions))
    simplex[0, :] = random.random(size=(dimensions, )) * span + lower_corner
    simplex[1:, :] = diag(random.uniform(-0.5, 0.5) * span) + simplex[0, :]

    values = function(*simplex.T)

    for i in range(maxiter):
        ilow, ihi, inh = interest_points(values)
        centroid = mean(delete(simplex, [ihi], axis=0), axis=0)
        reflected = move_towards(simplex[ihi, :], centroid, amount=2)
        rvalue = function(*reflected)

        if rvalue < values[ilow]:
            expanded = move_towards(simplex[ihi, :], centroid, amount=3)
            if function(*expanded) < rvalue:
                logging.debug('expand')
                simplex[ihi, :] = expanded
            else:
                logging.debug('reflect')
                simplex[ihi, :] = reflected
            continue

        # rvalue is implicitly not worst than values[ilow] at this point
        if rvalue < values[inh]:
            logging.debug('reflect')
            simplex[ihi, :] = reflected
            continue

        # rvalue is not better than values[inh] and values[ilow]
        contracted = move_towards(simplex[ihi, :], centroid, amount=0.5)
        if function(*contracted) < values[ihi]:
            logging.debug('contract')
            simplex[ihi, :] = contracted
            continue

        # contract everything to lowest

        logging.debug('multicontract')
        for j, source in enumerate(simplex):
            logging.debug(j, source)


if __name__ == "__main__":
    from functions import TestFunctionOne
    logging.basicConfig(
        format='%(levelname)s: %(message)s',
        level=logging.DEBUG)
    test = TestFunctionOne()
    nelder_and_mead(test, ((-1, -1), (1, 1)))
